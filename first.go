package main
import "fmt"

func main() {

	var a int = 5
	var b float32 = 4.32
	const pi float64 = 3.14159265

//	var(
//		c = 8
//		d=7
//	)
	// x=14,y=15 
	x, y := 14,15

	//fmt.Println(a) 
	fmt.Printf("Value of a = %d", a)
	fmt.Println(b)
	fmt.Println(pi)
	fmt.Println(12345/10)

	fmt.Println(x, ",", y)

	fmt.Println("x+y = ", x+y)
	fmt.Println("x-y = ", x-y)
	fmt.Println("x/y = ", x/y)
	fmt.Println("x*y = ", x*y)
	fmt.Println("x mod y = ", x%y)

	isbool := true
	hate := false
	fmt.Println(isbool && hate)
	fmt.Println(isbool || hate)
	fmt.Println(!isbool)

	// x = 10
	// changevalue(&x)
	// fmt.Println(x)

	for i:=1; i<5 ; i++{
		fmt.Println(i)
	}
}

// func changevalue(x *int) {

// 	*x = 7
// }




















