//This code takes three numbers as inputs and outputs which one is the highest, and which is lowest
//To get started with switches

package main

import "fmt"

func main(){

	fmt.Println("Give three numbers as input: ")

	var a int
	var b int
	var c int

	fmt.Print("Enter number: ")
	fmt.Scanln(&a)
	fmt.Print("Enter number: ")
	fmt.Scanln(&b)
	fmt.Print("Enter number: ")
	fmt.Scanln(&c)

	switch a>b{
	case true:
		switch b>c{
		case true:
			fmt.Println("Highest = a, lowest = c")
			break
		case false:
			switch a>c{
			case true:
				fmt.Println("Highest = a, lowest = b")
				break
			case false:
				fmt.Println("Highest = c, lowest = b")
				break
			}
		}
	case false:
		switch a>c{
		case true:
			fmt.Println("Highest = b, lowest = c")
			break
		case false:
			switch b>c{
			case true:
				fmt.Println("Highest = b, lowest = a")
				break
			case false:
				fmt.Println("Highest = c, lowest = a")
				break
			}
		}
	}
}