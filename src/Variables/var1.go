//Starting off with variables

package main

import "fmt"
import "reflect"

func main(){

	//declaring a variable, var <var_name> <var_type>
	var i int 

	//assiging a value
	i=30
	fmt.Println("i: ", i)

	//declarring a variable and assigning a value in the same line
	j := 30.245 //if type is undeclared, Go infers its own type
	fmt.Println("Type of j is", reflect.TypeOf(j)) //to check variable type, for some reason %T did not work
	var k float32 = 30.425
	fmt.Println("Type of k is", reflect.TypeOf(k)) //when using syntax like this, a space is inferred by go at the end of the first string

	fmt.Println("j:",j)
	fmt.Println("k:",k)

	//declaring multiple variables and assigning values in the same line
	var p, q = 25, "Hello"
	fmt.Println("p and q:", p, q)

	//declaring variables without "var" keyword, only applicable for variables that haven't already been declared
	a,b := 25,"Hello"
	fmt.Println("a and b: ", a, b)

	//constant variables are set with the keyword "constant" and cannot be changed again
	const c = 1
	fmt.Println("c: ", c)
	//c=5 will show cannot assign to c


}