//Learning how to take inputs in golang
//Issue: I cannot take string inputs with fmt.Scanln
//I can only take strings using bufio, reader, newstring etc 
//But even then when I am taking a string input, the string is stored in memory with the newline at the end
//Gotta get rid of this

package main

import "fmt"

func main(){

	//reading an integer and printing to console
	// var age int
	// fmt.Println("Enter your age: ")
	// fmt.Scanln(&age)
	// fmt.Println(age)

	// fmt.Println(name)

	// //Taking two strings as input and adding them together
	// fmt.Print("Enter first String: ")
	// var first string
	// fmt.Scan(&first)

	// fmt.Print("Enter second String: ")
	// var second string
	// fmt.Scan(&second)

	// fmt.Print(first+second)
	
}