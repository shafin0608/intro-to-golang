//Learning how to take inputs in golang
//Issue: I cannot take string inputs with fmt.Scanln
//I can only take strings using bufio, reader, newstring etc 
//But even then when I am taking a string input, the string is stored in memory with the newline at the end
//Gotta get rid of this

package main

import "fmt"
import "bufio"
import "os"

func main(){

	// //reading a string and printing to console
	// reader := bufio.NewReader(os.Stdin)
	// var name string 
	// fmt.Println("Enter your name: ")
	// name, _ = reader.ReadString('\n')

	// fmt.Println(name)

	//reading a string and printing to console
	reader := bufio.NewReader(os.Stdin)
	var first string 
	fmt.Println("Enter first string: ")
	first, _ = reader.ReadString('\n')

	var second string
	fmt.Println("Enter second string: ")
	second, _ = reader.ReadString('\n')

	fmt.Print(first+second)

}