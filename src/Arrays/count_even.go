//Count even and odd elements in an array

package main

import "fmt"

func main(){

	fmt.Println("Enter size of array: ")

	//selecting array size
	var n int
	fmt.Scanln(&n)

	//taking array input
	a := make([]int, n)

	for i := 0; i < n; i++ {
    	fmt.Scan(&a[i])
	}

	//printing inputted array
	for y := 0; y < n; y++ {
        fmt.Printf("%d ", a[y])
    }

    fmt.Println()
    var count = 0
    //looping over array
    for j:=0;j<len(a);j++{
    	if a[j]%2==0{
    		count++
    	}
    }

    fmt.Println("Number of even elements: ", count)   
    fmt.Println("Number of odd elements: ", len(a) - count)

}