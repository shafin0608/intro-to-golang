//Arrays and slices 
//Use the following for input
// a := make([]int, n)
// for i := 0; i < n; i++ {
//     fmt.Scan(&a[i])
// }

//Array represents a fixed size, named sequence of elements of the "same type".
//You cannot have an array which contains both integer and characters in it. 
//You cannot change the size of an array once You define the size

package main
import "fmt"

func main(){

	//declaring an array of strings
	var numbers [3] string
	//assigining values 
	numbers[0] = "One"
	numbers[1] = "Two"
	numbers[2] = "Three"

	fmt.Println("This will access the second element of array =>",numbers[1])
	fmt.Println("Length of array:",len(numbers))
	fmt.Println("Array of strings:", numbers)

	//declaring and initilazing together in the same line
	directions := [...] int{1,2,3,4,5}
	fmt.Println(directions)
	fmt.Println(len(directions))

	//A slice is a portion or segment of an array. Or it is a view or partial view of an underlying array to which it points.
	//Elements of a slice can be accessed using the slice name and index number just as you do in an array. 
	//You cannot change the length of an array, but you can change the size of a slice.


	//making a slice and making changes
	var b [] string = numbers[:2] //takes index 0 and 1, a[:n] means upto n-1
	//b:=numbers[:2] //alternative way of declaring and initializing

	fmt.Println("Before change, b:", b)
	fmt.Println("Before change, numbers:", numbers)


	b[0] = "changed"

	fmt.Println("After change, b:", b)
	fmt.Println("After change, numbers:", numbers)


	a := [...] string {"1","2","3","4","5"}
	slice_a := a[1:3]
	c := [...] string {"1","2","3","4","5"}
	slice_c := c[1:3]

	fmt.Println("New slice after appending c to a: ", append(slice_a, slice_c...)) 

	fmt.Println("New slice after appending a string to a: ", append(slice_a, "text1"))
}











