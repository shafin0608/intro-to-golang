//calculating sum of digits in a number
//this code is also written using pseudo-while loop like expressions

package main
import "fmt"

func main(){

	var num int
	fmt.Println("Enter a number: ")
	fmt.Scanln(&num)

	var sum int =0
	for num/10 >= 0{

		fmt.Println("Entering Loop")

		sum += num%10

		fmt.Println("Sum is now: ", sum)

		num /= 10

		fmt.Println("Number is now: ", num)

		//syntax of if condition, if condition 1{statements 1} elseif condition 2{statements 2} else{statements3}

		if num == 0{
			break
		}

	}

	//fmt.Println(sum)
}