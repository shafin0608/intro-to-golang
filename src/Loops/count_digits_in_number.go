//Counting the number of digits in a number

package main

import "fmt"

func main(){

	var num int
	fmt.Println("Enter your number: ")
	fmt.Scanln(&num)

	var count int = 1

	//since there is no way to explicitly write code using while loop
	//this is an alternative way to use for loop to perform similar to while syntax
	//while number/10 is not equal to zero, the loop will keep running

	for num/10!=0{
		count++
		num = num/10
	}

	fmt.Println(count)

}