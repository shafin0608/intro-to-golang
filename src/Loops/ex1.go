package main

import "fmt"

func main(){

	var i int

	fmt.Println("Enter a number: ")

	var n int

	fmt.Scan(&n)

	//syntax of for loop: for initialization;condition;increment{body}, no first brackets, no semicolons
	//there is no while loop in golang, only for loop

	for i=1;i<n;i++{
		fmt.Println(i)
	}

}