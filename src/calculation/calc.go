//package main
package calculation

// import "fmt"

// func main(){

// 	fmt.Println("This is nice")

// }

func Do_add(x int, y int) (int){

	return x+y
}

var Var1 int = 5 
//var var1 int = 5 //this will not export 

//noticed something unusual, if I uncomment line 17 and comment line 18, and do not go install calculation and do go run exe
//then it gives correct output
//does that mean I do not need to build/install my packages every time I make a change??

//if I uncomment line 17 and comment line 18, and follow same actions, now the output shows undefined variable calculation.Var1

