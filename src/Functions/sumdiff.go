package main

import "fmt"

func main(){

	var a,b int
	fmt.Print("Give first number: ")
	fmt.Scanln(&a)
	fmt.Print("Give second number: ")
	fmt.Scanln(&b)


	//function calling
	sum, diff := sumdiff(a,b)
	fmt.Printf("Sum and difference of a and b are %d and %d", sum, diff)


}

//function declaration func func_name(param_1 param_1 type, param_2 param_2 type) (ret_type1, ret_type2){body}
func sumdiff(a int, b int) (int, int){
	sum:=a+b
	diff:=a-b
	return sum,diff //function return
}